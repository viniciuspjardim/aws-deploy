import type { NextApiRequest, NextApiResponse } from "next";
import { generateNonce, brazilTimeString } from "@/utils";

type ResponseData = {
  name: string;
  at: string;
  nonce: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>
) {
  res.status(200).json({
    name: "John Doe",
    at: brazilTimeString(),
    nonce: generateNonce(),
  });
}
