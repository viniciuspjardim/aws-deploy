import Link from "next/link";
import Head from "next/head";
import { GetServerSideProps } from "next";

import { ClientTime } from "@/components/client-time";
import { GitLabLink } from "@/components/gitlab-link";
import { brazilTimeString, generateNonce } from "@/utils";

import styles from "@/page.module.css";
import { PageInfo } from "@/components/page-info";

export default function Home({
  serverTime,
  nonce,
}: {
  serverTime: string;
  nonce: string;
}) {
  console.log("Home (client) =>", {
    serverTime,
    nonce,
  });

  return (
    <div className={styles.container}>
      <Head>
        <title>Home - AWS Deploy</title>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1>Home</h1>

        <div style={{ display: "flex", marginBottom: "16px" }}>
          <Link href="/product/ball-a">Ball A</Link>

          <span>&nbsp;|&nbsp;</span>

          <Link href="/product/ball-b">Ball B</Link>

          <span>&nbsp;|&nbsp;</span>

          <Link href="/product/food-a">Food A</Link>

          <span>&nbsp;|&nbsp;</span>

          <Link href="/product/food-b">Food B</Link>
        </div>

        <PageInfo pageName="Home" serverTime={serverTime} nonce={nonce} />

        <p className={styles.info}>
          Test Sentry errors <Link href="/error-test">here</Link>.
        </p>

        <GitLabLink />
      </main>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps = async () => {
  const serverTime = brazilTimeString();
  const nonce = generateNonce();

  console.log("Home (server) =>", {
    serverTime,
    nonce,
  });

  return { props: { serverTime, nonce } };
};
