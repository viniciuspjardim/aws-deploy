import Link from "next/link";
import Head from "next/head";
import { useRouter } from "next/router";
import { GetServerSideProps } from "next";

import { PageInfo } from "@/components/page-info";
import { GitLabLink } from "@/components/gitlab-link";
import { brazilTimeString, generateNonce } from "@/utils";

import styles from "@/page.module.css";

export default function Product({
  serverTime,
  nonce,
}: {
  serverTime: string;
  nonce: string;
}) {
  const router = useRouter();
  const { slug } = router.query;

  console.log("Product (client) =>", {
    slug,
    serverTime,
    nonce,
  });

  return (
    <div className={styles.container}>
      <Head>
        <title>Product - AWS Deploy</title>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1>Product {slug}</h1>

        <div style={{ display: "flex", marginBottom: "16px" }}>
          <Link href="/">Home</Link>
        </div>

        <PageInfo
          pageName={`Product ${slug}`}
          serverTime={serverTime}
          nonce={nonce}
        />

        <p className={styles.info}>
          Product pages match all pages in <code>/product/*</code>.
        </p>

        <GitLabLink />
      </main>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const serverTime = brazilTimeString();
  const nonce = generateNonce();

  console.log("Product (server) =>", {
    slug: context?.query?.slug,
    serverTime,
    nonce,
  });

  return { props: { serverTime, nonce } };
};
