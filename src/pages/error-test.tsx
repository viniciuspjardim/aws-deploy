import Link from "next/link";
import Head from "next/head";
import { GetServerSideProps } from "next";

import { ClientTime } from "@/components/client-time";
import { ErrorSection } from "@/components/error-section";
import { GitLabLink } from "@/components/gitlab-link";
import { brazilTimeString, generateNonce } from "@/utils";

import styles from "@/page.module.css";
import { PageInfo } from "@/components/page-info";

export default function ErrorTest({
  serverTime,
  nonce,
}: {
  serverTime: string;
  nonce: string;
}) {
  console.log("Error Test (client) =>", {
    serverTime,
    nonce,
  });

  return (
    <div className={styles.container}>
      <Head>
        <title>Error Test - AWS Deploy</title>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1>Error Test</h1>

        <div style={{ display: "flex", marginBottom: "16px" }}>
          <Link href="/">Home</Link>
        </div>

        <PageInfo pageName="Error Test" serverTime={serverTime} nonce={nonce} />

        <ErrorSection />

        <GitLabLink />
      </main>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps = async () => {
  const serverTime = brazilTimeString();
  const nonce = generateNonce();

  console.log("Error Test (server) =>", {
    serverTime,
    nonce,
  });

  return { props: { serverTime, nonce } };
};
