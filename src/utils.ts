export function brazilTimeString() {
  return new Date()
    .toLocaleString("pt-BR", { timeZone: "America/Sao_Paulo" })
    .replace(",", "");
}

export function generateNonce() {
  return Math.floor(Math.random() * 1000000).toString();
}
