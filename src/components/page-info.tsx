import { ClientTime } from "@/components/client-time";

type PageInfoProps = {
  pageName: string;
  serverTime: string;
  nonce: string;
};

export function PageInfo({ pageName, serverTime, nonce }: PageInfoProps) {
  return (
    <>
      <code>
        Page<span style={{ opacity: "0" }}>_______</span>: {pageName}
        <br />
        Server Time: {serverTime}
        <br />
        Client Time: {<ClientTime />}
      </code>

      <p>{nonce}</p>
    </>
  );
}
