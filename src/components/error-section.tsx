import { useState } from "react";

const buttonStyle = {
  backgroundColor: "#c73852",
  margin: "0.5rem",
  border: "none",
  cursor: "pointer",
};

export function ErrorSection({}) {
  const [serverSuccessData, setServerSuccessData] = useState({});

  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <button
          type="button"
          style={buttonStyle}
          onClick={() => {
            throw new Error("Sentry client error test.");
          }}
        >
          Sentry Client Error
        </button>

        <button
          type="button"
          style={buttonStyle}
          onClick={async () => {
            await fetch("/api/server-error-test");
          }}
        >
          Sentry Server Error
        </button>

        <button
          type="button"
          style={{
            ...buttonStyle,
            backgroundColor: "#146526",
          }}
          onClick={async () => {
            const response = await fetch("/api/foo");
            setServerSuccessData(await response.json());
          }}
        >
          Server Success
        </button>
      </div>

      <code
        style={{
          margin: "1rem",
          fontFamily: "monospace",
          textAlign: "center",
        }}
      >
        {JSON.stringify(serverSuccessData, null, 2)}
      </code>
    </>
  );
}
