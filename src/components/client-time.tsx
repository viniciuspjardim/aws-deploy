"use client";

import { useEffect, useState } from "react";
import { brazilTimeString } from "@/utils";

export function ClientTime() {
  const [clientTime, setClientTime] = useState("Loading...");

  useEffect(() => {
    const time = brazilTimeString();
    setClientTime(time);

    console.log("ClientTime =>", time);
  }, []);

  return <>{clientTime}</>;
}
